﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Авторизация	</title>
	<link rel="stylesheet" href="style/style1.css">
	<link rel="shortcut icon" href="images/1.png" type= "image/png">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900&display=swap&subset=cyrillic" rel="stylesheet">
</head>
<body background="images/1.png">
	<div class="container">
		<h1 class="vhod1">Авторизация</h1>
		<div class="reg_form">
		<?php 
		if(!isset($error))
			echo $error;
		 ?>	
		<form  method="post">
			<p>Логин: <input name="login" type="text" style="padding: 4px; border-radius: 6px;" ></p>
			<p> Пароль: <input name="password" type="password" style="padding: 4px; border-radius: 6px;" ></p>
			<a href="index.php" class="vhod">Войти</a>
			<div><a href="index.php"class="forgot-password">Забыли пароль?</a></div>
		</form>
	</div>
	</div>
</div>
</body>
</html>